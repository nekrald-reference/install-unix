#!/usr/bin/env bash

set -uexo 

# Auxiliary libraries for providing background.
pip3 install --user --upgrade argcomplete # For pip and pipx compromise.

# Requested libraries for easy start.
pip3 install --user bs4 
pip3 install --user click
pip3 install --user cvxopt
pip3 install --user cvxpy
pip3 install --user dateparser
pip3 install --user dill
pip3 install --user faker
pip3 install --user jupyter
pip3 install --user jupyterlab
pip3 install --user keras
pip3 install --user lxml
pip3 install --user matplotlib 
pip3 install --user networkx 
pip3 install --user numpy 
pip3 install --user orderedset
pip3 install --user pandas 
pip3 install --user pyclustering
pip3 install --user pymongo
pip3 install --user pyomo
pip3 install --user python-docx 
pip3 install --user scipy 
pip3 install --user seaborn
pip3 install --user selenium 
pip3 install --user scikit-learn
pip3 install --user statsmodels
pip3 install --user tensorflow[and-cuda]
pip3 install --user tqdm 
pip3 install --user xlrd
pip3 install --user xlwt
pip3 install --user xlsxwriter 
pip3 install --user pandoc
pip3 install --user pypandoc
pip3 install --user markdown2pdf3
pip3 install --user django
pip3 install --user joblib
pip3 install --user gym[atari,box2d,classic_control]
pip3 install --user pylint
pip3 install --user dash
pip3 install --user flask
pip3 install --user boto3
pip3 install --user boto3-stubs

set +uexo
